# Load packages

library(tidyverse)

# Load data

datafolder <- c("~/Dokumente/Daten/Tom Davidson/NP guardrails/gitlab/long-term-nutrient-chla-links-shallow-lakes/Data/Source data/DK data") # set data source path

dk <- read_csv(file.path(datafolder, "big_ass_data.csv"), fileEncoding = "ISO-8859-1") # load lake data, with Danish file encoding due to special Danish letters
dk_coord <- read.csv(file.path(datafolder,"DK_lakes_coord.csv"), fileEncoding = "ISO-8859-1") # load coordinate data, with Danish file encoding due to special Danish letters

# Keep only distinct lake numbers (soenr), as multiple observation sites may exist in one lake, coordinates of lakes should be still valid
dk_coord_distinct <- distinct(dk_coord, soenr, .keep_all = TRUE)

# Use left join to link files based on lake number (soenr)
new_lake <- left_join(x = dk, y = dk_coord_distinct, by = "soenr")

# some checks
str(new_lake)
str(dk)
nrow(new_lake)
nrow(dk)

# export combined dataframe
write_csv(new_lake, file = file.path(datafolder, "DK_lake_data_with_coord.csv"))

