library(tidyverse)

# datafolder <- c("C:/Daten/Tom Davidson/alternative stable states/")
datafolder <- c("~/Dokumente/Daten/Tom Davidson/alternative stable states/")
outfolder <- c("~/Dokumente/Daten/Tom Davidson/NP guardrails/")
# datafolder <- c("/home/daniel/Dokumente/Daten/Tom Davidson/alternative stable states/")

setwd(datafolder)
bigass <- read_csv("big_ass_data.csv")#, show_col_types = FALSE)


# Prepare data  ----
# calculate ratios and organic N and P, and some other ratios for the manuscript
bigass2 <- bigass %>% 
  filter(kond < 2000) %>% 
  drop_na(chla, ptot, ntot) %>% 
  mutate(n_p = ntot / 14 / ptot * 31) %>% 
  mutate(log_n_p = log(ntot / 14 / ptot * 31)) %>% 
  mutate(n_p_mass = ntot / ptot) %>%
  mutate(po4_ptot = po4 / ptot) %>% 
  mutate(no3_ntot = no3 / ntot) %>% 
  mutate(ton = ntot - no3) %>% 
  mutate(top = ptot - po4) %>% 
  mutate(area = areal) %>% 
  dplyr::select(-areal) %>%
  filter(is.na(no3) == FALSE | is.na(po4) == FALSE)

# a histogram
hist(bigass2$sigt)

# Check number of lakes
shallow <- bigass2 %>% 
  filter(dybmid < 6) %>% 
  dplyr::select(soenr) %>% 
  unique() %>% 
  nrow()

all_depths <- bigass2 %>% 
  dplyr::select(soenr) %>% 
  unique() %>% 
  nrow()

shallow; all_depths; shallow / all_depths

# final selection of lakes < 6 m
bigass3 <- bigass2 %>% 
  filter(dybmid < 6)

bigass2 <- bigass3

# site vector for loop
site_vec <- unique(bigass2$soenr)

cons.yr <- NULL # Create empty matrix
for(i in 1:length(site_vec)) { # Do the following for each lake factor level.
  sub <- bigass2 %>% filter(soenr == site_vec[i]) # Create subset just containing one lake
  
  if(nrow(sub)>1) { # ifelse in case subsets just contain one year
    # if more than one year is contained, do the following
    sub.before <- sub$year[2:length(sub$year)] 
    sub.after <- sub$year[1:(length(sub$year)-1)] 
    diff.before <- sub.before - sub$year[1:(length(sub$year)-1)] # calculate differences (hence distances) between years before
    diff.after <- sub$year[2:length(sub$year)] - sub.after
    loop <- cbind(c(999, diff.before), c(diff.after, 999)) # the first and last year gets a high value, since there is either no year before or after to the first or last year in diff.before / diff.after
    
  } else { # else ;-)
    loop <- cbind(999,999) # only one year is existing, it gets a high value to later exclude it
  }
  
  # bind the lake info, years and differences together(cbind) and write it into the same rows of the empty matrix as it was in the original dataset
  cons.yr <- rbind(cons.yr, cbind(sub$soenr, sub$year, loop)) #lonr is a factor, which needs to be converted back to integer, since cons.yr is an integer matrix
}

# rename final consecutive year columns
bigass3 <- bigass2 %>% 
  mutate(cons_yr = cons.yr[,4]) %>% 
  mutate(cons_yr_bf = cons.yr[,3])


# renaming and deleting of not needed data
bigass <- bigass3; rm(bigass2, bigass3)

# check for duplicates
bigass2 <- bigass[duplicated(paste(bigass$soenr, bigass$year)) != T,]
bigass <- bigass2; rm(bigass2)

# prepare and export final data
bigassdata <- bigass %>% 
  dplyr::select(soenr, year, chla, ntot, ptot, po4, no3, dybmid, area, n_p:cons_yr_bf)

setwd(outfolder)
write_csv(bigassdata, "dk_data_inorganic.csv")

