# Prepare the data, calculate means

# for detailled script comments see "mean calculation_non bootstrap.R"

# load libraries ----
library(tidyverse)
library(foreach)
library(doParallel)
library(modeest)
library(HDInterval)

# register folders ----
# datafolder <- c("C:/Daten/Tom Davidson/alternative stable states/")
datafolder <- c("~/Dokumente/Daten/Tom Davidson/NP guardrails/")
# datafolder <- c("/home/daniel/Dokumente/Daten/Tom Davidson/alternative stable states/")

# load data ----
setwd(datafolder)
us_data <- read_csv("lagosne_data.csv")#, show_col_types = FALSE)

afila_data <- read_csv("afila_data.csv") %>% dplyr::select(soenr, year, chla, ntot, ptot, dybmid, n_p, log_n_p, n_p_mass, cons_yr, cons_yr_bf)

dk_data <- read_csv("dk_data.csv")# %>% dplyr::select(-dybmid)

d <- rbind(dk_data, afila_data)

d %>% filter(cons_yr == 0)
d %>% filter(cons_yr_bf == 0)

# load functions -----
# Function to assess whether and how much consecutive years are contained within the data, allowing a specified data gaps
# ass = source data
# n_means = planned number of simple moving average years, this function only works for multi-year means
# gap = gap years, 
    # e.g. gap = 3 means that three years of gap are allowed. 
    # gap = 1 means that difference between years = 1, hence, no gap is allowed

yr_sel <- function(ass = assdata, n_means = 3, gap = 3) {
  
  # calulate valid consecutive years, only done for each mean year subset -----
  if(n_means > 1) {
    
    # Select for years whose distances to the year measured before or after are smaller than the predefined gap, in some rare cases in the Danish data, multiple growing season means with different values were available for the same lake and year. As the true value is unknown in this case, those values are excluded by the cons_yr > 0 or cons_yr_bf > 0 filtering step.
    presel_sites <- ass[which(ass$cons_yr <= gap & ass$cons_yr > 0 | ass$cons_yr_bf <= gap & ass$cons_yr_bf > 0),]    
    
    # select only lakes with more than x consecutive years    
    assi_count <- presel_sites %>% count(soenr) # count number of consecutive years
    assi_count2 <- assi_count %>% filter(n >= n_means)
    sel_sites <- presel_sites %>% filter(soenr %in% assi_count2$soenr) 
    
    # This loop generates a vector based on which the bootstrap function determines sub-datasets with sufficient years for mean generation for each lake
    site_vec <- unique(sel_sites$soenr) # make vector with unique lake number for loop
    all_split_groups <- NULL; c_all <- NULL # create empty vectors for output 
    for(j in 1:length(site_vec)) {
      sub <- sel_sites %>% filter(soenr == site_vec[j]) # select one lake
      
      t <- c(0, diff(sub$year)) # calculate year difference between observations of one lake, with a starting 0
      t2 <- t > gap # create a true/false vector for values which are larger than the pre-defined year gap
      t3 <- as.numeric(t2) # make the true/false vector a 1/0 vector
      
      split_groups <- rep(1, length(t3)); splitter <- 0; counter <- 1; c_loop <- rep(1, length(t3)) # Create empty vector for year sub-groups within lake time series
      for(k in 2:(length(t3))) { # for all years of each lake
        counter  <- ifelse(t3[k] == 1, 1, counter + 1) # count the number of years potentially belonging to the same mean calculation sub-group, to make sure that mean calculations are only attempted for subgroups with sufficient years (no. years in sub-group >= no. of years for mean calculation)
        splitter <- ifelse(t3[k] == 1, splitter + 1, splitter + 0) # determine splits, if t3 is not 1 (hence splitter == 1), hence, if the number of gap years is larger than the gap criterion, otherwise no split (splitter == 0)
        split_groups[k] <- split_groups[k] + splitter # add split values to the split_groups vector
        c_loop[k] <- counter # add count values to count vector
      }
      
      c_all <- c(c_all, c_loop) # add lake specific count vector to global count vector
      all_split_groups <- c(all_split_groups, split_groups) # add lake specific split vector to global split vector
    }
    sel_sites2 <- cbind(sel_sites, all_split_groups, c_all) # column bind count and split vectors with lake data 
    
    sel_sites3 <- sel_sites2 %>% 
      group_by(soenr, all_split_groups) %>% # group data by lake and split sub-groups
      filter(max(c_all) >= n_means) # only keep those split sub-groups which have the same or more members than used for multi-year mean generation
    
    sel_sites4 <- ungroup(sel_sites3) # ungroup again, to avoid issues with later data treatment
    
    return(sel_sites4) # return ungrouped data
  } else return("Only use this function for data with 2 or more year means.") # To make sure that this function is only used for multi-year means
}

# Function to calculate SMA years after assessment of consecutive years
# ass = data input, which is output from function above
# n_means = planned number of simple moving average years, this function only works for multi-year means

mean_calc <- function(ass = d_red, n_means = 3) {
  
  # function to calculate sum of absolute difference between mean and annual value, to be used in SI assessment of ideal number of SMA years
  sum_diff <- function(x) sum(abs(mean(x) - x))

# select unique lakes
site_vec <- unique(ass$soenr)
  
  # foreach with %dopar% runs the loop on multiple CPUs, runs slightly faster than on a single core (depends on PC in use)
  all_fin <- foreach(i = 1:length(site_vec), .combine = rbind, .packages = c("dplyr", "zoo"), .verbose = FALSE) %dopar% { # for each of the lakes
    # select a lake j and create subset
    sub <- ass %>% filter(soenr == site_vec[i]) # select a lake
    
    split_groups <- sub %>% pull(all_split_groups) %>% unique() # keep unique split groups (e.g. a lake dataset contains two time series of data with a break in between for which SMAs can be calculated separately)
    
    fin <- NULL # output data to be filled
    for(j in 1 : length(split_groups)) {
      sub2 <- sub %>% filter(all_split_groups == split_groups[j]) # select a split group
      
      # for each split group calculate all possible means and export a data frame with the data to the upper loop
      for(k in 1 : (nrow(sub2)-(n_means-1))) {
        sub3 <- sub2[k : (k + (n_means-1)),]
        
        min_year <- min(sub3$year)
        max_year <- max(sub3$year)
        
        sel_loop <- sub3 %>% 
          dplyr::select(soenr, dybmid, ptot, ntot, n_p, log_n_p, n_p_mass, chla) %>% 
          mutate(min_yr = min_year) %>% 
          mutate(max_yr = max_year)


       sum_loop <- sel_loop %>% 
          dplyr::select(-soenr, -dybmid, -min_yr, -max_yr) %>% 
       colSums(rollapply(., width = n_means, sum_diff)) %>% 
         t() %>% as_tibble()
               
               
       names(sum_loop) <- paste0(names(sel_loop %>% 
                                        dplyr::select(-soenr, -dybmid, -min_yr, -max_yr)), "_sumdiff")
       
        out_loop <- apply(sel_loop, 2, mean) %>% t() %>% as_tibble()
        
        out_loop <- cbind(out_loop, sum_loop) %>% mutate(Mean_years = n_means)
        
        # out_loop <- apply(sel_loop, 2, mean)
        
        fin <- rbind(fin, out_loop)
      }
    }
  fin
  }
  all_fin <- as_tibble(all_fin)
  return(all_fin)
}

# # global iteration vars ----

# Cores for parallel processing
# #~~~~~~~~~~~~~~~~~~~~~~~~~~~
# #~~~~~~~~~~~~~~~~~~~~~~~~~~~
# # register cores -----
cores <- 8 # select number of CPU cores based on your PC
cl <- makePSOCKcluster(cores)
registerDoParallel(cl)
# #~~~~~~~~~~~~~~~~~~~~~~~~~~~
# #~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Number of gap years
g <- 1 #years, 1 gap year means that no missing year is allowed


# 1-year data ----
# no SMA calculation here, only for comparison
s1 <- d %>% 
  dplyr::select(soenr, year, ptot, ntot, n_p, log_n_p, n_p_mass, chla)

# procedure is the same for all mean (SMA) years, but check 5-year means (SMAs) for calculation of 5-year SMA residuals
# all mean calculations are needed later to provide evidence of ideal SMA length

# 2-year data ----
d_red <- yr_sel(ass = d, n_means = 2, gap = g) # select consecutive years
m <- mean_calc(ass = d_red, n_means = 2) # calculate 2-year SMAs

# create lake-min-may-year vector
m <- m %>% 
  mutate(uniq = paste(.$soenr, .$min_yr, .$max_yr, sep = "_")) %>% 
  mutate(lake_chr = as.character(soenr))

# final 2-year SMA data to be saved later
m2 <- m 

# same data as for 2-year SMAs but without mean calculation (for potential comparison)
s2 <- d_red %>% 
  dplyr::select(soenr, ptot, ntot, n_p, log_n_p, n_p_mass, chla)



# 3-year means ----
d_red <- yr_sel(ass = d, n_means = 3, gap = g)
m <- mean_calc(ass = d_red, n_means = 3)

m <- m %>% 
  mutate(uniq = paste(.$soenr, .$min_yr, .$max_yr, sep = "_")) %>% 
  mutate(lake_chr = as.character(soenr))

m3 <- m 

s3 <- d_red %>% 
  dplyr::select(soenr, ptot, ntot, n_p, log_n_p, n_p_mass, chla)


# 4-year means ----
d_red <- yr_sel(ass = d, n_means = 4, gap = g)
m <- mean_calc(ass = d_red, n_means = 4)

m <- m %>% 
  mutate(uniq = paste(.$soenr, .$min_yr, .$max_yr, sep = "_")) %>% 
  mutate(lake_chr = as.character(soenr))

m4 <- m 

s4 <- d_red %>% 
  dplyr::select(soenr, ptot, ntot, n_p, log_n_p, n_p_mass, chla)

setwd(datafolder)
save(s4, m4, file = "test means.Rdata")

# 5-year means ----
d_red <- yr_sel(ass = d, n_means = 5, gap = g)
m <- mean_calc(ass = d_red, n_means = 5)

m <- m %>% 
  mutate(uniq = paste(.$soenr, .$min_yr, .$max_yr, sep = "_")) %>% 
  mutate(lake_chr = as.character(soenr))

# generate new mean dataset with middle year column for residual calculation (middle year is needed to link to the correct growing season mean)
m_hi <- m %>% mutate(middle_yr = round(max_yr - ((max_yr - min_yr))/2),1)

m5 <- m
m5 %>% .$soenr %>% unique %>% length
m5 %>% .$soenr %>% length

s5 <- d_red %>% 
  dplyr::select(soenr, year, ptot, ntot, n_p, log_n_p, n_p_mass, chla) 

# generate new single-year dataset with renamed middle year column to link to mean (SMA) data
s_hi <- s5 %>%
  mutate(middle_yr = year)

nrow(s5) # check number of cases
s5 %>% .$soenr  %>% unique %>% length # check number of lakes

# high-bandpass filter table
# join single and mean data based on lake number and middle year
hi <- inner_join(m_hi, s_hi, by = c("soenr", "middle_yr"), suffix = c(".5y", ".1y"))

hi %>% .$soenr %>% unique() %>% length()  # check number of lakes
hi %>% .$soenr %>%  length() # check number of cases per lake

any(duplicated(paste(hi$soenr, hi$middle_yr))) # check whether there are any duplicates, which should not be the case

# subtract 5-year SMA from growing season observation, calulate TN : TP and log TN : TP for 5-year SMA
hi <- hi %>%
          mutate(ntot = ntot.1y - ntot.5y, 
                  ptot = ptot.1y - ptot.5y,
                  chla = chla.1y - chla.5y,
                  n_p = ntot.5y / 14 / ptot.5y * 31,
                  log_n_p = log(ntot.5y / 14 / ptot.5y * 31)) %>%
          dplyr::select(soenr, dybmid, year, ntot : log_n_p)

# plot produced data for checking
hist(hi$ptot)
plot(ntot ~ log_n_p, hi)
plot(ptot ~ log_n_p, hi)

# final data to be used in further analyses
hi5 <- hi          

# 6-year means ----
d_red <- yr_sel(ass = d, n_means = 6, gap = g)
m <- mean_calc(ass = d_red, n_means = 6)

m <- m %>% 
  mutate(uniq = paste(.$soenr, .$min_yr, .$max_yr, sep = "_")) %>% 
  mutate(lake_chr = as.character(soenr))

m6 <- m 

s6 <- d_red %>% 
  dplyr::select(soenr, ptot, ntot, n_p, log_n_p, n_p_mass, chla)

# 7-year means ----
d_red <- yr_sel(ass = d, n_means = 7, gap = g)
m <- mean_calc(ass = d_red, n_means = 7)

m <- m %>% 
  mutate(uniq = paste(.$soenr, .$min_yr, .$max_yr, sep = "_")) %>% 
  mutate(lake_chr = as.character(soenr))

m7 <- m 

s7 <- d_red %>% 
  dplyr::select(soenr, ptot, ntot, n_p, log_n_p, n_p_mass, chla)

# 8-year means ----
d_red <- yr_sel(ass = d, n_means = 8, gap = g)
m <- mean_calc(ass = d_red, n_means = 8)

m <- m %>% 
  mutate(uniq = paste(.$soenr, .$min_yr, .$max_yr, sep = "_")) %>% 
  mutate(lake_chr = as.character(soenr))

m8 <- m 

s8 <- d_red %>% 
  dplyr::select(soenr, ptot, ntot, n_p, log_n_p, n_p_mass, chla)

# 9-year means ----
d_red <- yr_sel(ass = d, n_means = 9, gap = g)
m <- mean_calc(ass = d_red, n_means = 9)

m <- m %>% 
  mutate(uniq = paste(.$soenr, .$min_yr, .$max_yr, sep = "_")) %>% 
  mutate(lake_chr = as.character(soenr))

m9 <- m 

s9 <- d_red %>% 
  dplyr::select(soenr, ptot, ntot, n_p, log_n_p, n_p_mass, chla)

# 10-year means ----
d_red <- yr_sel(ass = d, n_means = 10, gap = g)
m <- mean_calc(ass = d_red, n_means = 10)

m <- m %>% 
  mutate(uniq = paste(.$soenr, .$min_yr, .$max_yr, sep = "_")) %>% 
  mutate(lake_chr = as.character(soenr))

m10 <- m 

s10 <- d_red %>% 
  dplyr::select(soenr, ptot, ntot, n_p, log_n_p, n_p_mass, chla)

# 11-year means ----
d_red <- yr_sel(ass = d, n_means = 11, gap = g)
m <- mean_calc(ass = d_red, n_means = 11)

m <- m %>% 
  mutate(uniq = paste(.$soenr, .$min_yr, .$max_yr, sep = "_")) %>% 
  mutate(lake_chr = as.character(soenr))

m11 <- m 

s11 <- d_red %>% 
  dplyr::select(soenr, ptot, ntot, n_p, log_n_p, n_p_mass, chla)

# 12-year means ----
d_red <- yr_sel(ass = d, n_means = 12, gap = g)
m <- mean_calc(ass = d_red, n_means = 12)

m <- m %>% 
  mutate(uniq = paste(.$soenr, .$min_yr, .$max_yr, sep = "_")) %>% 
  mutate(lake_chr = as.character(soenr))

m12 <- m 

s12 <- d_red %>% 
  dplyr::select(soenr, ptot, ntot, n_p, log_n_p, n_p_mass, chla)

# 13-year means ----
d_red <- yr_sel(ass = d, n_means = 13, gap = g)
m <- mean_calc(ass = d_red, n_means = 13)

m <- m %>% 
  mutate(uniq = paste(.$soenr, .$min_yr, .$max_yr, sep = "_")) %>% 
  mutate(lake_chr = as.character(soenr))

m13 <- m 

s13 <- d_red %>% 
  dplyr::select(soenr, ptot, ntot, n_p, log_n_p, n_p_mass, chla)

# 14-year means ----
d_red <- yr_sel(ass = d, n_means = 14, gap = g)
m <- mean_calc(ass = d_red, n_means = 14)

m <- m %>% 
  mutate(uniq = paste(.$soenr, .$min_yr, .$max_yr, sep = "_")) %>% 
  mutate(lake_chr = as.character(soenr))

m14 <- m 

s14 <- d_red %>% 
  dplyr::select(soenr, ptot, ntot, n_p, log_n_p, n_p_mass, chla)

# separate saves of .csv files for Julia bootstrap procedure (as R workspaces are more difficult to load into Julia)
write_csv(x = m5, file = "m5.csv")
write_csv(x = s1, file = "s1.csv")
write_csv(x = s5, file = "s5.csv")
write_csv(x = hi5, file = "hi5.csv")

# Saving R workspace for later use
setwd(datafolder)
save(s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, hi5, file = "calculated means.Rdata")
