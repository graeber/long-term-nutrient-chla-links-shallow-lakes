# Input data columns
# soenr = unique lake number / ID
# chla = Chlorophyll a concentration in µg / L 
# ntot = total nitrogen concentration in mg / L (tn)
# ptot = total phosphorus concentration in mg / L (tp)
# log_n_p = natural log TN : TP ratio
#
# Output data columns
# cept_tp = intercept of chla ~ tp model (GLM, with Gamma distribution of Chla)
# slop_tp = slope of chla ~ tp model (µg Chla per mg TP)
# r2_tp = pseudo R² of chla ~ tp model, calculated as  1 - (model deviance / null_deviance)
# aic_tp = AIC of chla ~ tp model
# cept_tn = intercept of chla ~ tn model (GLM, with Gamma distribution of Chla)
# slop_tn = slope of chla ~ tn model (µg Chla per mg TN)
# r2_tn = pseudo R² of chla ~ tn model, calculated as  1 - (model deviance / null_deviance)
# aic_tn = AIC of chla ~ tn model (µg Chla per mg TN)
# cept_tptn_add = intercept of chla ~ tp + tn model (GLM, with Gamma distribution of Chla)
# slop_tptn_tp_add = tp slope of chla ~ tp + tn model (µg Chla per mg TP)
# slop_tptn_tn_add = tn slope of chla ~ tp + tn model (µg Chla per mg TN)
# r2_tptn_add = pseudo R² of chla ~ tp + tn model, calculated as  1 - (model deviance / null_deviance)
# aic_tptn_add = AIC of chla ~ tp + tn model
# cept_tptn_inter = intercept of chla ~ tp * tn model (GLM, with Gamma distribution of Chla)
# slop_tptn_tp_inter = tp slope of chla ~ tp * tn model (µg Chla per mg TP)
# slop_tptn_tn_inter = tn slope of chla ~ tp * tn model (µg Chla per mg TN)
# slop_tptn_tn_inter = tp*tn slope of chla ~ tp * tn model (µg Chla per mg TP*TN)
# r2_tptn_inter = pseudo R² of chla ~ tp * tn model, calculated as  1 - (model deviance / null_deviance)
# aic_tptn_inter = AIC of chla ~ tp * tn model
# mean_log_tntp = mean log TN : TP of random sample, 
# mean_ntot = mean TN concentration of random sample, mg / L
# mean_ptot  = mean TP concentration of random sample, mg / L
# mean_chla = mean Chla concentration of random sample,  µg / L 
# n = number of observations in random sample
#
#
# To install and update packages, uncomment this code.
#= using Pkg # start package manager
Pkg.add("CSV")
Pkg.add("DataFrames")
Pkg.add("DataFramesMeta")
Pkg.add("GLM")
Pkg.add("IJulia")
Pkg.add("Plots") 
Pkg.update() # update packages
 =#
#= Pkg.rm("DataFramesMeta")
Pkg.rm("DataFrames") =#

# Use the following packages 
using DataFrames, DataFramesMeta, GLM, Statistics

# Function to split data into windows, randomly sample from the windows, and to calculate generalized linear models based on Gamma distribution
# inputdata = input dataframe
# b = number of iterations per window
# min_window =  natural log TN:TP of minimum window
# max_window =  natural log TN:TP of maximum window
# window_width =  natural log TN:TP window width
# window_step =  natural log TN:TP of window stepwise movement

function mov_win(inputdata; b = 100, min_window = 0, max_window = 7.5, window_width = 3, window_step = 0.1) 
  
    min_win = [min_window : window_step : max_window - window_width;] # calculate lower natural log TN:TP window boundaries
    max_win = [min_window + window_width : window_step : max_window;] # calculate upper natural log TN:TP window boundaries
    
    u = Threads.ReentrantLock(); # lock variable for parallel processing used for handling synchronization and ensuring that for loop is only executed by a single thread at a time, especially important for the shared data structure to avoid race conditions.

    all_out = Array{Float64}(undef, 0, 20) # empty array to be filled with final data in mov_out
    Threads.@threads for i in 1:b # FOR loop with parallel processing threads based on system settings, e.g. Julia extension settings in VSCode / VSCodium


        mov_out = Array{Float64}(undef, 0, 20)  # empty array to be filled with result of all iterations within TN:TP window
        for mov in 1:lastindex(min_win) # for each TN : TP window
            
            # filter data only within TN : TP window
            lim_np = @chain inputdata begin
                filter(:log_n_p => >(min_win[mov]), _)
                filter(:log_n_p => <(max_win[mov]), _)
            end


            uni_soenr = unique(lim_np[:,:soenr]) # make a list of unique lake IDs (soenr)
            site_vec_b = rand(uni_soenr, lastindex(uni_soenr)) # take a random sample with replacement from the unique lake IDs
            
            # Create a random sample with replacment at lake ID level and a single random observation within the selected random lake
            smpl_n = similar(lim_np,0) # initialize an empty, fillable array of same type and number as input columns
            for j in 1:lastindex(site_vec_b) # return last index of site_vec_b
                sub = filter(:soenr => ==(site_vec_b[j]), lim_np) # filter one lake
                subrow = sub[rand(1:nrow(sub)),:] # pick a single random observation from the filtered lake
                push!(smpl_n,subrow) # push observation to smpl_n
            end
            
            # "try" stops the code whenever an error occurs, in this case if a model does not converge or if the null deviance is lower than the model deviance, hence if the GLM model predicts the data even worse than a constant
            try
                loop_res = Array{Float64}(undef,1,20) # create an empty array for the statistics results
                null_deviance = deviance(glm(@formula(chla ~ 1), smpl_n, Gamma(), IdentityLink())) # calculate NULL deviance, assuming a constant Chla
                
                # GLM for chla ~ tp

                glm_chla_tp = glm(@formula(chla ~ ptot), smpl_n, Gamma(), IdentityLink()) # GLM with Gamma distribution and sample identity link type
                loop_res[1] = GLM.coef(glm_chla_tp)[1] # cept_tp = intercept of chla ~ tp model (GLM, with Gamma distribution of Chla)
                loop_res[2] = GLM.coef(glm_chla_tp)[2] # slop_tp = slope of chla ~ tp model (µg Chla per mg TP)
                loop_res[3] = 1 - (deviance(glm_chla_tp) / null_deviance) # r2_tp = pseudo R² of chla ~ tp model, calculated as  1 - (model deviance / null_deviance)
                loop_res[4] = 2 * 3 - 2 * loglikelihood(glm_chla_tp) # AIC = 2K – 2ln(L) K = number of model parameters, the default value of K is 2, so a model with just one predictor variable will have a K value of 2+1 = 3; ln(L) = The log-likelihood of the model. Most statistical software can automatically calculate this value for you. https://www.statology.org/aic-in-r/

                # GLM for chla ~ tn
                glm_chla_tn = glm(@formula(chla ~ ntot), smpl_n, Gamma(), IdentityLink())
                loop_res[5] = GLM.coef(glm_chla_tn)[1] # cept_tn = intercept of chla ~ tn model (GLM, with Gamma distribution of Chla)
                loop_res[6] = GLM.coef(glm_chla_tn)[2] # slop_tn = slope of chla ~ tn model (µg Chla per mg TN)
                loop_res[7] = 1 - (deviance(glm_chla_tn) / null_deviance) # r2_tn = pseudo R² of chla ~ tn model, calculated as  1 - (model deviance / null_deviance)
                loop_res[8] = 2 * 3 - 2 * loglikelihood(glm_chla_tn) # aic_tn = AIC of chla ~ tn model (µg Chla per mg TN)
                
                # GLM for chla ~ tp + tn
                glm_chla_add = glm(@formula(chla ~ ptot + ntot), smpl_n, Gamma(), IdentityLink())
                loop_res[9] = GLM.coef(glm_chla_add)[1] # cept_tptn_add = intercept of chla ~ tp + tn model (GLM, with Gamma distribution of Chla)
                loop_res[10] = GLM.coef(glm_chla_add)[2] # slop_tptn_tp_add = tp slope of chla ~ tp + tn model (µg Chla per mg TP)
                loop_res[11] = GLM.coef(glm_chla_add)[3] # slop_tptn_tn_add = tn slope of chla ~ tp + tn model (µg Chla per mg TN)
                loop_res[12] = 1 - (deviance(glm_chla_add) / null_deviance) # r2_tptn_add = pseudo R² of chla ~ tp + tn model, calculated as  1 - (model deviance / null_deviance)
                loop_res[13] = 2 * 3 - 2 * loglikelihood(glm_chla_add) # aic_tptn_add = AIC of chla ~ tp + tn model

                # GLM for chla ~ tp * tn, extract R² and AIC for testing purposes (does not perform better than additive model)
                glm_chla_tptn_inter = glm(@formula(chla ~ ptot * ntot), smpl_n, Gamma(), IdentityLink())
                loop_res[14] = 1 - (deviance(glm_chla_tptn_inter) / null_deviance) # r2_tptn_inter = pseudo R² of chla ~ tp * tn model, calculated as  1 - (model deviance / null_deviance)
                loop_res[15] = 2 * 3 - 2 * loglikelihood(glm_chla_tptn_inter) # aic_tptn_inter = AIC of chla ~ tp * tn model

                loop_res[16] = mean(log.(smpl_n[:,:ntot] / 14 ./ smpl_n[:,:ptot] * 31)) # mean_log_tntp = mean log TN : TP of random sample, 
                loop_res[17] = mean(smpl_n[:,:ntot]) # mean_ntot = mean TN concentration of random sample, mg / L
                loop_res[18] = mean(smpl_n[:,:ptot]) # mean_ptot  = mean TP concentration of random sample, mg / L
                loop_res[19] = mean(smpl_n[:,:chla]) # mean_chla = mean Chla concentration of random sample,  µg / L 
                
                loop_res[20] = nrow(smpl_n) # n = number of observations in random sample
                
                mov_out = [mov_out; loop_res]    
            catch
            end
                    
      end   
      Threads.lock(u) do
      all_out = [all_out; mov_out]
      end
    end 

    df_out = DataFrame(all_out, [:cept_tp, :slop_tp, :r2_tp, :aic_tp, :cept_tn, :slop_tn, :r2_tn, :aic_tn, :cept_tptn_add, :slop_tptn_tp_add, :slop_tptn_tn_add, :r2_tptn_add, :aic_tptn_add, :r2_tptn_inter, :aic_tptn_inter, :mean_log_tntp, :mean_ntot, :mean_ptot, :mean_chla, :n])

    return df_out
end


# Function to split data into windows, randomly sample from the windows, and to calculate generalized linear models based on **NORMAL** distribution
# Apart from the normal distribution, all comments above apply here

function mov_win_norm(inputdata; b = 100, min_window = 0, max_window = 7.5, window_width = 3, window_step = 0.1)
  
    min_win = [min_window : window_step : max_window - window_width;]
    max_win = [min_window + window_width : window_step : max_window;]
    
    u = Threads.ReentrantLock();

    all_out = Array{Float64}(undef, 0, 20)
    Threads.@threads for i in 1:b #:static 

        mov_out = Array{Float64}(undef, 0, 20) 
        for mov in 1:lastindex(min_win)
            
            lim_np = @chain inputdata begin
                filter(:log_n_p => >(min_win[mov]), _)
                filter(:log_n_p => <(max_win[mov]), _)
            end


            uni_soenr = unique(lim_np[:,:soenr])
            site_vec_b = rand(uni_soenr, lastindex(uni_soenr))

            smpl_n = similar(lim_np,0)
            for j in 1:lastindex(site_vec_b)
                sub = filter(:soenr => ==(site_vec_b[j]), lim_np)
                subrow = sub[rand(1:nrow(sub)),:]
                push!(smpl_n,subrow)
            end
            
            try
                loop_res = Array{Float64}(undef,1,20)
                null_deviance = deviance(glm(@formula(chla ~ 1), smpl_n, Normal(), IdentityLink()))
               
                glm_chla_tp = glm(@formula(chla ~ ptot), smpl_n, Normal(), IdentityLink())
                loop_res[1] = GLM.coef(glm_chla_tp)[1]
                loop_res[2] = GLM.coef(glm_chla_tp)[2]
                loop_res[3] = 1 - (deviance(glm_chla_tp) / null_deviance)
                loop_res[4] = 2 * 3 - 2 * loglikelihood(glm_chla_tp) 
                
                glm_chla_tn = glm(@formula(chla ~ ntot), smpl_n, Normal(), IdentityLink())
                loop_res[5] = GLM.coef(glm_chla_tn)[1]
                loop_res[6] = GLM.coef(glm_chla_tn)[2]
                loop_res[7] = 1 - (deviance(glm_chla_tn) / null_deviance)
                loop_res[8] = 2 * 3 - 2 * loglikelihood(glm_chla_tn)

                glm_chla_add = glm(@formula(chla ~ ptot + ntot), smpl_n, Normal(), IdentityLink())
                loop_res[9] = GLM.coef(glm_chla_add)[1]
                loop_res[10] = GLM.coef(glm_chla_add)[2]
                loop_res[11] = GLM.coef(glm_chla_add)[3]
                loop_res[12] = 1 - (deviance(glm_chla_add) / null_deviance)
                loop_res[13] = 2 * 3 - 2 * loglikelihood(glm_chla_add)

                glm_chla_tptn_inter = glm(@formula(chla ~ ptot * ntot), smpl_n, Normal(), IdentityLink())
                loop_res[14] = 1 - (deviance(glm_chla_tptn_inter) / null_deviance)
                loop_res[15] = 2 * 3 - 2 * loglikelihood(glm_chla_tptn_inter)

                loop_res[16] = mean(smpl_n[:,:log_n_p])
                loop_res[17] = mean(smpl_n[:,:ntot])
                loop_res[18] = mean(smpl_n[:,:ptot])
                loop_res[19] = mean(smpl_n[:,:chla])
                
                loop_res[20] = nrow(smpl_n)
                
                mov_out = [mov_out; loop_res]    
            catch
            end
                    
      end   
      Threads.lock(u) do
      all_out = [all_out; mov_out]
      end
    end 

    df_out = DataFrame(all_out, [:cept_tp, :slop_tp, :r2_tp, :aic_tp, :cept_tn, :slop_tn, :r2_tn, :aic_tn, :cept_tptn_add, :slop_tptn_tp_add, :slop_tptn_tn_add, :r2_tptn_add, :aic_tptn_add, :r2_tptn_inter, :aic_tptn_inter, :mean_log_tntp, :mean_ntot, :mean_ptot, :mean_chla, :n])

    return df_out
end

