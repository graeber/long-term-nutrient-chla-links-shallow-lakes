# Mean (simple moving averages / SMA) data

This folder contains the results of the mean_calculation_ ... .R scripts found in [Mean (SMA) calculation scripts](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/blob/main/Mean%20(SMA)%20calculation%20scripts)


The "calculated means.Rdata" and the "calculated means_inorganic.Rdata" are used in the main manuscript.

The csv files are used in the Julia scripts for the hierarchical bootstrap procedure and GLM calculation. See [Hierarchical bootstrap scripts](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/blob/main/Hierarchical%20bootstrap%20scripts).