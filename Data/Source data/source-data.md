# Source data

## DK data
This data was downloaded from https://odaforalle.au.dk/. Growing season means were calculated in an earlier study (https://www.nature.com/articles/s41467-023-36043-9) and are used here.

**Three files exist in DK source data:**
1. Water quality data (big_ass_data.csv)
2. Lake coordinate data (DK_lakes_coord.csv)
3. Merge of coordinates and water quality data (DK_lake_data_with_coord.csv)

## Filazzola et al 2020
This data was downloaded from  https://doi.org/10.1038/s41597-020-00648-2. When depths were missing, depths from the HydroLAKES database were used.  
[Link to the files](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/blob/main/Data/Source%20data). Here "afila_ChlaData.csv" is the original file and "afila_hydro.csv" is the csv file with added depths from HydroLAKES.

## LAGOS NE
Provided by the LAGOSNE package. Downloaded directly into R. See [LAGOS-NE preparation script](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/blob/main/Data%20preparation%20scripts/data_prep_lagosne_test_inorganic.R) for details.

# Workflow 
**This data is loaded into the data preparation scripts (data_prep_ ... .R).**