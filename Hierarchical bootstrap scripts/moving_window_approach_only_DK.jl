# Moving window approach

using Pkg, CSV, DataFrames, DataFramesMeta # load package
include("$(homedir())/Dokumente/Daten/R scripts/np guardrails/functions/tn_tp window and bootstrap function.jl") # load bootstrap function (in gitlab to be found  in directory "Functions used in other scripts")

homedir()  # check what the home directory is
cd("$(homedir())/Dokumente/Daten/Tom Davidson/NP guardrails/")

# load csv with data, produced by "mean_calculation_non_bootstrap_only_DK.R"
m5 = CSV.read("m5_dk.csv", DataFrame)
hi5 = CSV.read("hi5_dk.csv", DataFrame)

# Run bootstrap function @time gives used time in seconds and used resources, for function parameters see function script
@time tm5 = mov_win(m5, b = 300)
@time thi5 = mov_win_norm(hi5, b = 300)

# add columns for later data analysis and plotting
tm5 = @chain tm5 begin
    @transform(:type = "mean", :mean_years = 5)
end

thi5 = @chain thi5 begin
    @transform(:type = "high_pass", :mean_years = 5)
end

# generate data frame containing all data
all_results = [tm5; thi5]

# calculate delta AIC and relative AIC
all_results = @chain all_results begin
    @transform :delta_aic_add_tp = :aic_tptn_add .- :aic_tp
    @transform :delta_aic_add_tn = :aic_tptn_add .- :aic_tn
    @transform :delta_aic_inter = :aic_tptn_inter .- :aic_tptn_add
    @transform :rel_aic_add_tp = :aic_tptn_add ./ :aic_tp
    @transform :rel_aic_inter = :aic_tptn_inter ./ :aic_tptn_add
end

# keep only reasonable data with R² being equal or greater than 0 and equal and smaller than 1
all_results = @subset all_results begin 
        :r2_tp .>= 0.00
        :r2_tp .<= 1.00
        :r2_tptn_add .>= 0
        :r2_tptn_add .<= 1
        :r2_tptn_inter .>= 0
        :r2_tptn_inter .<= 1
    end

# Export CSV
CSV.write("moving NP random sampling_bootstrap_Julia_only_DK.csv", all_results)