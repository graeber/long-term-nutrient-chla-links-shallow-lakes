# Readme on the code and further supplementary information for "Consistent stoichiometric long-term relationships between nutrients and chlorophyll-a across lakes"

## Research paper

The information of this repository is used in a as of yet unpublished manuscript. Code and data are available for review purposes only.

## Used data sources

1. Filazzola, A., Mahdiyan, O., Shuvo, A., Ewins, C., Moslenko, L., Sadid, T., Blagrave, K., Imrit, M.A., Gray, D.K., Quinlan, R., O’Reilly, C.M., Sharma, S., 2020. A database of chlorophyll and water chemistry in freshwater lakes. Sci Data 7, 310. https://doi.org/10.1038/s41597-020-00648-2  
This publication is a compilation of other open-access data and contains total nitrogen, total phosphorus and chlorophyll a concentrations. Data can be downloaded from [](https://doi.org/10.5063/F1RV0M1S).
2. The [LAGOS-NE](https://lagoslakes.org/products/data-products/) database contains data from the North-Eastern USA. Data is open-access. Information on the dataset can be found in [Soranno et al. (2017)](https://academic.oup.com/gigascience/article/6/12/gix101/4555226?)
3. The [Overfladevandsdatabasen (surface water database)](https://odaforalle.au.dk/login.aspx) contains data from Denmark. Data is open-access. The conditions for using this data can be found on the website of the Overfladevandsdatabasen.
4. Lake depth data was downloaded from the open-access database [HydroLAKES](https://www.hydrosheds.org/products/hydrolakes).


## Data workflow

1. Data has been downloaded or been taken from an earlier publication. For source 1, missing lake depth data was substituted from the HydroLAKES database (source 1). 
    - Source data are found [here](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/tree/main/Data/Source%20data).

2. Data has been prepared to unify data structure, units, lake ID structure, and column names. Total nitrogen, total phosphorus and chlorophyll a data was downloaded from source 1, to which nutrient (total nitrogen (TN) and total phosphorus (TP) and chlorophyll a was added. Mean depth data was used to select for shallow lakes with average depth < 6 m. For the Danish data, only freshwater lakes with < 2000 µS / cm were selected. To assess, whether stoichiometric saturation of N or P exerted an effect on inorganic, potentially reactive nutrient concentrations, we extracted further data. Inorganic nutrient data was not available from source 1, but from source 2 and 3. Here, we compiled the data in the same way as the total nutrient data. 
    - Prepared data are found [here](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/tree/main/Data/Prepared%20data).
    - Scripts to prepare data are found [here](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/tree/main/Data%20preparation%20scripts).
    - **If you want to use the R code files, you need to change the folder variables.**

3. Consecutive years in time series were filtered and for those simple moving averages and their residuals calculated. Only time series with 5 or more consecutive years were selected and growing season means calculated. 5-year simple moving averages and their residuals were calculated (see Methods of manuscript for detailed description). 
    - The compiled datasets with the original lake ID, years, the averages and their residuals for TN, TP and Chla concentrations can be downloaded [here](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/tree/main/Data/Mean%20(SMA)%20data).
    - The scripts to calculate simple moving averages and their residuals can be found [here](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/tree/main/Data/Mean%20(SMA)%20data).
    - **If you want to use the R code files, you need to change the folder variables.**

4. We used a hierarchical bootstrap procedure to assess the relationships between total nutrients and chlorophyll a along a total nutrient ratio gradient. For this we used generalized linear models. Please see the methods and SI of the article for details. This bootstrap was done in Julia, not in R, as the bootstrap procedure in Julia ran over 100 times faster. 
    - The results of the bootstrap runs differ slightly between each run. Do not expect to get exactly the same results but all the reported patterns should be highly similar due to the high number of bootstrap iterations (see the Methods section of the article for details). 
        - Bootstrap results used in the main article can be found [here](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/tree/main/Data/Main%20article%20bootstrap%20results).
        - Bootstrap results used in the SI can be found [here](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/tree/main/Data/SI%20bootstrap%20results).
    - To generate your own runs, here are the commented Julia scripts for
        - [the bootstrap function itself](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/tree/main/Functions%20used%20in%20other%20scripts)
        - [the application and collection of data from the bootstrap function](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/tree/main/Hierarchical%20bootstrap%20scripts)
    - **If you want to use the Julia files, you need to change the folder variables.**

5. The [manuscript](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/blob/main/main-manuscript.qmd) and [SI](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/blob/main/supplementary-information.qmd) were written in [Quarto Markdown](https://quarto.org/). This includes the text, R chunks with calculations, simulations and plots, cross-references and literature references. These files use data and functions provided in the following folders:
    - [Main article bootstrap results](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/tree/main/Data/Main%20article%20bootstrap%20results)
    - [SI bootstrap results](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/tree/main/Data/SI%20bootstrap%20results)
    - [Mean (SMA) data](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/tree/main/Data/Mean%20(SMA)%20data)
    - [Scatter plot function provided in Functions folder](https://git.ufz.de/graeber/long-term-nutrient-chla-links-shallow-lakes/-/tree/main/Functions%20used%20in%20other%20scripts)
    - **If you want to use the qmd files, you need to change the folder variables in the second and third R chunk.**

### R version and packages

R version 4.3 was used in the data analyses.

The R code within this repository only uses the following R libraries, all available in [CRAN](https://cran.r-project.org):

1. [tidyverse](https://cran.r-project.org/web/packages/tidyverse/)
2. [foreach](https://cran.r-project.org/web/packages/foreach)
3. [doParallel](https://cran.r-project.org/web/packages/doParallel/)
4. [diptest](https://cran.r-project.org/web/packages/diptest/)
5. [ggpubr](https://cran.r-project.org/web/packages/ggpubr/)
6. [patchwork](https://cran.r-project.org/web/packages/patchwor/)
7. [LAGOSNE](https://cran.r-project.org/web/packages/LAGOSNE/)


### Julia version and packages

Julia version 1.9.0 was used in the data analyses.

All packages are available through Julia's builtin package manager "Pkg":

1. [DataFrames](https://dataframes.juliadata.org/stable/)
2. [DataFramesMeta](https://juliadata.github.io/DataFramesMeta.jl/stable/)
3. [CSV](https://csv.juliadata.org/stable/)
4. [Pkg](https://docs.julialang.org/en/v1/stdlib/Pkg/)
5. [GLM](https://github.com/JuliaStats/GLM.jl)
6. [Statistics](https://github.com/JuliaStats)



## Contact for questions
Please contact **Daniel Graeber** (daniel.graeber[at]ufz.de) if you have questions

## License
This project is licensed under the BSD 3-Clause "New" or "Revised" License.

